# Rails Evaluation Project
> REST course API (based on http://jsonapi.org/format/ spec)., async logger

### Run application in local environment

1. run `bundle install`
2. run foreman start
2. run `rails s`
3. Go to ```http://localhost:3000```

### Application uses the following services

1. Redis (https://redislabs.com/, redis-17252.c10.us-east-1-2.ec2.cloud.redislabs.com:17252, railstest)
2. MongoDB (https://mlab.com/, mongodb://rails-test:railstestPassword1@ds157248.mlab.com:57248/rails_test_db)
3. RabbitMQ (https://www.cloudamqp.com/, 	amqp://ejmvjpni:AqU9wJxXWi7_fcLfoYXfwen-CIWYhFgJ@hyena.rmq.cloudamqp.com/ejmvjpni)
4. Splunk (https://www.splunk.com/, https://prd-p-2qlf8pwt5vng.cloud.splunk.com, B9A0155F-BBE4-4CAA-BDD9-37D42C6EB126)

### Production Logs

1. Go to https://www.splunk.com/
2. Login rails_test/railstestPassword
3. Go to Instances
4. Go to INSTANCE **logs** ACCESS INSTANCE (https://prd-p-2qlf8pwt5vng.cloud.splunk.com/)
5. Go to Search & Reporting (https://prd-p-2qlf8pwt5vng.cloud.splunk.com/en-US/app/launcher/home)

### API

```REST Course JSONAPI.postman_collection.json```

class Api::BaseController < ActionController::Base
  include Api

  rescue_from Mongoid::Errors::DocumentNotFound do |e|
    render json: to_jsonapi(nil, links: { self: request.original_url }), status: :not_found
  end
end

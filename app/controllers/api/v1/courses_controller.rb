class Api::V1::CoursesController < Api::BaseController
  before_action :set_course, only: [:update, :destroy]
  before_action :cache,      only: [:index, :show]

  def index
    @courses = Course.all
    if stale? @courses
      render json: to_jsonapi(@courses, { is_collection: true })
    end
  end

  def show
    @course = Course.find(params[:id])
    if stale? @course
      render json: to_jsonapi(@course)
    end
  end

  def create
    @course = Course.new(course_params)

    if @course.save
      render json: to_jsonapi(@course), status: :created
    else
      render json: to_jsonapi(@course.errors, {}, true), status: :unprocessable_entity
    end
  end

  def update
    if @course.update(course_params)
      render json: to_jsonapi(@course)
    else
      render json: to_jsonapi(@course.errors, {}, true), status: :unprocessable_entity
    end
  end

  def destroy
    @course.destroy
    head 204
  end

  private
    def set_course
      @course = Course.find(params[:id])
    end

    def course_params
      params
        .require(:data)
        .require(:attributes)
        .permit(
          :name,
          :author
        )
    end

    def cache
      expires_in(1.day)
    end
end

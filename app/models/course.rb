class Course
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name,   type: String
  field :author, type: String

  validates :name, :author, presence: true
end

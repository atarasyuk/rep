class CourseSerializer < BaseSerializer
  attribute :name
  attribute :author
end

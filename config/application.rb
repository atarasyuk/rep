require_relative 'boot'

require "rails"
require "active_model/railtie"
require "action_controller/railtie"
require "action_view/railtie"
require "action_mailer/railtie"
require "action_cable/engine"
require "rails/test_unit/railtie"

Bundler.require(*Rails.groups)

module Rep
  class Application < Rails::Application
    REDIS = config_for(:redis)

    config.autoload_paths << Rails.root.join('lib')
    config.redis_config = REDIS
    config.redis_config_url = "redis://#{ REDIS['username'] }:#{ REDIS['password'] }@#{ REDIS['host'] }:#{ REDIS['port'] }/#{ REDIS['db'] }"

    config.api_only = true

    config.generators do |g|
      g.orm :mongoid
    end

    config.middleware.use Rack::Attack
  end
end

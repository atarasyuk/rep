Rails.application.configure do
  config.cache_classes = true
  config.eager_load    = true

  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  config.cache_store = :redis_store, Rails.application.config_for(:redis)
  config.action_dispatch.rack_cache = {
    metastore:   "#{ config.redis_config_url }/metastore",
    entitystore: "#{ config.redis_config_url }/entitystore"
  }

  config.public_file_server.enabled = true

  config.i18n.fallbacks = true
  config.active_support.deprecation = :notify
  config.logger = AsyncLogger.new
end

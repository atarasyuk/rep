module JSONAPI
  MIMETYPE = 'application/vnd.api+json'
end

Mime::Type.register(JSONAPI::MIMETYPE, :json, %W(application/vnd.api+json text/x-json application/json))

ActionDispatch::Http::Parameters::DEFAULT_PARSERS[
  Mime::Type.lookup(JSONAPI::MIMETYPE)
] = -> (body) { JSON.parse(body) }

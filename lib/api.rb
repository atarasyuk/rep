module Api
  def to_jsonapi(model, options = {}, is_errors = false)
    if is_errors
      JSONAPI::Serializer.serialize_errors(model)
    else
      JSONAPI::Serializer.serialize(model, { is_collection: false }.merge(options))
    end
  end
end

module AsyncLogger
  CONFIG   = Rails.application.config_for(:async_logger)
  RABBITMQ = CONFIG['rabbitmq']
  SPLUNK   = CONFIG['splunk']

  def self.new
    begin
      log_report = LogReport.new
      logger = Logger.new(log_report)
      logger.formatter = log_report.formatter
      logger
    rescue Exception => e
      puts "AsyncLogger: #{ e.message }".red
    end
  end
end

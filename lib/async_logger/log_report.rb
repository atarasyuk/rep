module AsyncLogger
  class LogReport

    def initialize
      @publisher = AsyncLogger::Publisher.new
    end

    def write(message)
      @publisher.send(message)
    end

    def close
    end

    def formatter
      Proc.new do |severity, datetime, progname, message|
        "pid=#{ Process.pid } | #{ severity } | #{ datetime } | #{ message } \n"
      end
    end

  end
end

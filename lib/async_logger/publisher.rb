module AsyncLogger
  class Publisher

    def initialize
      @config = AsyncLogger::RABBITMQ

      connect
      create_exchange
    end

    def send(message)
      begin
        @exchange.publish(message)
      rescue Interrupt
        puts "AsyncLogger::Publisher: Interrupted".red
      end
    end

    def close
      @connection.close
    end

    private

    def create_exchange
      channel   = @connection.create_channel
      queue     = channel.queue(@config['queue'])
      @exchange = Bunny::Exchange.new(
        channel,
        @config['exchange_type'],
        @config['exchange'],
        { durable: @config['durable'] }
      )
      queue.bind(@exchange, routing_key: @config['queue'])
    end

    def connect
      begin
        @connection = Bunny.new({
          host:     @config['host'],
          vhost:    @config['vhost'],
          user:     @config['user'],
          password: @config['password']
        })

        @connection.start
      rescue Exception => e
        puts "AsyncLogger::Publisher: #{ e.message }".red
      end
    end

  end
end

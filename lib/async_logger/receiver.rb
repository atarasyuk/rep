module AsyncLogger
  class Receiver
    include Sneakers::Worker
    include AsyncLogger::SplunkLogger

    from_queue(
      AsyncLogger::RABBITMQ['queue'],
      {
        durable: AsyncLogger::RABBITMQ['durable'],
        timeout_job_after: 5,
        threads: 1,
        start_worker_delay: 5
      }
    )

    def work(payload)
      send(payload)
      ack!
    end
  end
end

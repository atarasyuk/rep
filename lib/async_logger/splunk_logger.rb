module AsyncLogger
  module SplunkLogger
    extend self

    def send(message)
      config = AsyncLogger::SPLUNK

      Typhoeus::Request.new(
        "https://input-#{ config['host'] }/services/collector",
        method: :post,
        ssl_verifypeer: false,

        body: {
          event: message,
          host: config['app_host'],
          sourcetype: config['sourcetype'],
          source: config['source'],
          index: config['index'],
          time: Time.now.to_i
        }.to_json,

        headers: {
          'Content-Type': 'application/json',
          'Authorization': "Splunk #{ config['token'] }"
        }
      ).run
    end

  end
end

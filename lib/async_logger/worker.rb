require "sneakers/runner"

module AsyncLogger
  class Worker
    def initialize
      configure
    end

    def configure
      config = AsyncLogger::RABBITMQ

      Sneakers.configure(
        amqp: "amqp://#{ config['user'] }:#{ config['password'] }@#{ config['host'] }",
        vhost: config['vhost'],
        exchange: config['exchange'],
        exchange_type: config['exchange_type'],
        log: 'log/sneakers.log',
        heartbeat: 10,
        metrics: nil,
        daemonize: false,
        workers: 1
      )
      Sneakers.logger.level = Logger::FATAL
    end

    def run
      begin
        Sneakers::Runner.new([ AsyncLogger::Receiver ]).run
      rescue Exception => e
        puts "AsyncLogger::Worker: #{ e.message }".red
      end
    end
  end
end

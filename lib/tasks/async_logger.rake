namespace :async_logger do
  desc "Run AsyncLogger worker"
  task :worker => :environment do
    AsyncLogger::Worker.new.run
  end
end

require 'test_helper'
require "json"

class Api::V1::CoursesControllerTest < ActionDispatch::IntegrationTest

  setup do
    @course = create(:course)
  end

  test "should get courses" do
    get api_v1_courses_url
    res  = JSON.parse(response.body)

    assert_response :success
    assert_kind_of Array, res['data']
  end

  test "should get course" do
    get api_v1_course_url(@course)

    assert_response :success
    assert_equal JSON.parse(response.body), JSONAPI::Serializer.serialize(@course)
  end

  test "should create new course" do
    post(api_v1_courses_url, params: {
      data: {
        attributes: { name: 'Course Name', author: 'Course Author' }
      }
    })

    res = JSON.parse(response.body)

    assert_response 201
    assert_equal 'Course Name',   res['data']['attributes']['name']
    assert_equal 'Course Author', res['data']['attributes']['author']
  end

  test "should get errors when trying create course with invalid data" do
    post(api_v1_courses_url, params: {
      data: {
        attributes: { name: '', author: '' }
      }
    })

    errors = JSON.parse(response.body)['errors']

    assert_response 422
    assert_equal "Name can't be blank",   errors.first['detail']
    assert_equal "Author can't be blank", errors.last['detail']
  end

  test "should delete course" do
    count = Course.count - 1
    delete api_v1_course_url(@course)

    assert_response 204
    assert_equal count, Course.count
  end

  test "should update course" do
    patch(api_v1_course_url(@course), params: {
      data: {
        attributes: { name: 'New Course Name', author: 'New Course Author' }
      }
    })

    res = JSON.parse(response.body)

    assert_response 200
    assert_equal 'New Course Name',   res['data']['attributes']['name']
    assert_equal 'New Course Author', res['data']['attributes']['author']
  end

  test "should get errors when trying update course with invalid data" do
    post(api_v1_courses_url, params: {
      data: {
        attributes: { name: '', author: '' }
      }
    })

    errors = JSON.parse(response.body)['errors']

    assert_response 422
    assert_equal "Name can't be blank",   errors.first['detail']
    assert_equal "Author can't be blank", errors.last['detail']
  end

  test "should get empty data when requesting course with invalid ID" do
    get api_v1_course_url({ id: 'invalid id' })

    res = JSON.parse(response.body)

    assert_response 404
    assert_nil res['data']
  end
end

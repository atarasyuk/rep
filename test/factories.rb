FactoryGirl.define do
  factory :course, class: Course do
    name   'Course Name'
    author 'Course Author'
  end
end